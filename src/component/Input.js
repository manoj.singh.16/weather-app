import React, { Component } from "react";

export class Input extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: "",
    };
  }

  handleChange = (e) => {
    this.setState({
      value: e.target.value,
    });
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.props.getWeatherByCity(this.state.value);
  };

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <input
          type="text"
          placeholder="Enter City"
          value={this.state.value}
          onChange={this.handleChange}
        />

        <button type="submit">Get Weather</button>
      </form>
    );
  }
}

export default Input;
