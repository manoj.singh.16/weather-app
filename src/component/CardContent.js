import React, { Component } from "react";

export class CardContent extends Component {
  render() {
    const { name, main, weather } = this.props.weatherData;
    return (
      <>
        <h2>Name: {name}</h2>
        <h2>temp: {main.temp}</h2>
        <h2>desc: {weather[0].description}</h2>
        <img
          src={`https://openweathermap.org/img/wn/${weather[0].icon}@2x.png`}
          alt="weather-icon"
        />
      </>
    );
  }
}

export default CardContent;
